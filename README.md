# Core-Projekt

Im Projekt werden alle gemeinsamen Anforderungen für drupal-basierte Projekte der LWB gesammelt.

Anforderungen für alle hausinternen Projekte sind der Import von Stammdaten, eine automatische Anmeldung der Nutzer innerhalb des LWB-Netzes und eine Wiedererkennbarkeit des Anwendungen über ein gemeinsames Design.

Es soll eine Stringenz in den Projekten gewährleistet werden.

## Voraussetzungen

* [Drupal 10](https://www.drupal.org/10)
* [Adminimal Admin Toolbar](https://www.drupal.org/project/adminimal_admin_toolbar)
* [CAS](https://www.drupal.org/project/cas)
* [Search API Solr](https://www.drupal.org/project/search_api_solr)
* [Masquerade](https://www.drupal.org/project/masquerade)
* [Drush](https://www.drush.org/)
* [CDI](https://gitlab.com/lwb-web/drupal/modules/cdi/)
* [Huari](https://gitlab.com/lwb-web/drupal/themes/huari)

## Systemvoraussetzungen

* PHP >= 8.2
    * php-cli php-mcrypt php-intl php-mysql php-curl php-gd php-soap php-xml php-zip
* MySQL >= 5.5.3 OR MariaDB >= 10.3
* Composer >= 2.0
